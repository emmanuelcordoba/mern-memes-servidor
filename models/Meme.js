const mongoose = require('mongoose');
 
// Definimos el Schema
const MemeSchema = mongoose.Schema({
    titulo: {
        type: String,
        required: true,
        trim: true
    },
    url: {
        type: String,
        required: true,
        trim: true
    },
    imgBase64: { 
        type: String, 
        required: true,
    },
    created_at: {
        type: Date,
        default: Date.now()
    }
});
 
// Definimos el modelo Meme con el schema correspondiente
module.exports = mongoose.model('Meme', MemeSchema);