require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const Meme = require('./models/Meme');

// Conexion a la BD
const URI = 'mongodb+srv://tester:MLpNVea2dXSdRjR7@cluster-fslbn.mongodb.net/memes?retryWrites=true&w=majority';
const OPTIONS = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
};
 
mongoose.connect(URI,OPTIONS)
    .then(db => console.log('Base de datos conectada'))
    .catch(error => {
        console.error(error);
        process.exit(1); // Detener la App
    });

// Creamos el servidor
const app = express();

// Puerto del servidor
const PORT = process.env.PORT || 4000;

// Middlewares
app.use(express.json({limit: '10mb'})); // for parsing application/json
app.use(express.urlencoded({ limit: '10mb', extended: true})); // for parsing application/x-www-form-urlencoded
app.use(cors());

// Definimos las RUTAS

// Agregar un meme
app.post('/memes', async (req, res) => {
    try {
        // Crear el meme
        let meme = new Meme(req.body);

        // Guardamos el meme en la BD
        await meme.save();
        res.json({ msg: 'El Meme fue creado correctamente', meme });
    } catch (error) {
        console.error(error);
        return res.status(500).json({ msg: 'Hubo un error'});
    }
});

// Listar memes
app.get('/memes', async (req, res) => {
    try {
        // Traemos todos los memes de la BD
        let memes = await Meme.find();      
        res.json({ memes });
    } catch (error) {
        console.error(error);
        return res.status(500).json({ msg: 'Hubo un error'});
    }
});

// Borrar un meme
app.delete('/memes/:id', async (req, res) => {
    try {
        // Validar el ID
        if(!mongoose.Types.ObjectId.isValid(req.params.id)){
            return res.status(404).json({ msg: 'El Meme no existe'});
        }

        // Verificar que exista el Meme
        let meme = await Meme.findById(req.params.id);

        if(!meme){
            return res.status(404).json({ msg: 'El Meme no existe'});
        }

        // Eliminar
        await meme.remove();
        res.json({ msg: 'El Meme fue eliminado con éxito.'})
    } catch (error) {
        console.error(error);
        return res.status(500).json({ msg: 'Hubo un error'});
    }
})

// Iniciar el servidor
app.listen(PORT, '0.0.0.0', () => {
    console.log(`El servidor está funcionando en el puerto ${PORT}`);
});